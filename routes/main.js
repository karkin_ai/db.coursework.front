const express = require('express');
const router = express.Router();
const content = require('../config/content.json');
const settings = require('../config/settings.json');

let common = {
  role: content.const.designer,
  appSvcUrl: settings.appSvcUrl + ':' + settings.appSvcPort,
  consts: content.const,
  username: content.header.username,
  avatar: content.header.profile_photo,
  company:  content.const.company,
  search_placeholder: content.const.search_placeholder,
  search_label: content.const.search,
  mypage_label: content.const.my_page,
  edit_label: content.const.edit,
  logout_label: content.const.logout,
  signin_label: content.const.signin,
  signup_designer_label: content.const.signup_designer,
  signup_showroom_label: content.const.signup_showroom,
  signup_label: content.const.signup
};

common.navitems = content.header.navbar.navitems.map(obj => {
  return {
    label: content.const[obj['title']],
    page: obj.page,
    href: obj.link
  }
});

/* GET home page. */
router.get('/', function(req, res, next) {
  let vars = Object.assign(common, {
    title: content.const.company + " - " + content.const.homepage,
    page: 'homepage',
    isAuthorized: false,
  });
  vars.navitems.forEach(item => { item.klass = 'nav-item ' + (item.page === vars.page ? 'active' : '') });
  res.render('homepage/homepage', vars);
});


/* GET clothes catalog. */
router.get('/clothes', function(req, res, next) {
  let vars = Object.assign(common, {
    title: content.const.company + " - " + content.const.clothes,
    page: 'clothes',
    isAuthorized: true
  });
  vars.navitems.forEach(item => { item.klass = 'nav-item ' + (item.page === vars.page ? 'active' : '') });
  res.render('clothes/clothes', vars);
});


/* GET showrooms. */
router.get('/showrooms', function(req, res, next) {
  let vars = Object.assign(common, {
    title: content.const.company + " - " + content.const.homepage,
    page: 'showrooms',
    isAuthorized: true
  });
  vars.navitems.forEach(item => { item.klass = 'nav-item ' + (item.page === vars.page ? 'active' : '') });
  res.render('showrooms/showrooms', vars);
});


/* GET designers. */
router.get('/designers', function(req, res, next) {
  let vars = Object.assign(common, {
    title: content.const.company + " - " + content.const.homepage,
    page: 'designers',
    isAuthorized: true
  });
  vars.navitems.forEach(item => { item.klass = 'nav-item ' + (item.page === vars.page ? 'active' : '') });
  res.render('designers/designers', vars);
});


/* GET sign in page. */
router.get('/signin', function(req, res, next) {
  let vars = Object.assign(common, {
    title: content.const.company + " - " + content.const.homepage,
    page: 'signin',
    isAuthorized: false
  });
  vars.navitems.forEach(item => { item.klass = 'nav-item ' + (item.page === vars.page ? 'active' : '') });
  res.render('signin/signin', vars);
});


/* GET designers sign up. */
router.get('/signup_designer', function(req, res, next) {
  let vars = Object.assign(common, {
    title: content.const.company + " - " + content.const.homepage,
    page: 'signup_designers',
    isAuthorized: false
  });
  vars.navitems.forEach(item => { item.klass = 'nav-item ' + (item.page === vars.page ? 'active' : '') });
  res.render('signup/signup-designer', vars);
});


/* GET showrooms sign up. */
router.get('/signup_showroom', function(req, res, next) {
  let vars = Object.assign(common, {
    title: content.const.company + " - " + content.const.homepage,
    page: 'signup_showroom',
    isAuthorized: false
  });
  vars.navitems.map(item => Object.assign(item, { klass: 'nav-item ' + (item.page === vars.page ? 'active' : '') }));
  res.render('signup/signup-showroom', vars);
});

router.get('/upload', function(req, res, next) {
  res.render('upload', {});
});


module.exports = router;
