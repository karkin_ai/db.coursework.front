let express = require('express');
let router = express.Router();
let fs = require('fs');
let path = require('path');
let settings = require('../config/settings.json');

router.get('/settings', function(req, res, next) {
  res.send(JSON.stringify(settings));
});


router.post('/upload', function(req, res, next) {
  let img = req.body.image;
  let imgName = req.body.email + ".jpg";
  let imgB64 = img.replace(/^data:([A-Za-z-+/]+);base64,/, '');
  let imgBinary  = new Buffer(imgB64, 'base64').toString('binary');
  console.log(req.body);
  fs.writeFile(path.join(__dirname, "../uploads/", imgName), imgBinary, 'binary', function(err) {
    if(err) console.log(err);
  });
  res.redirect('/');
});

module.exports = router;
